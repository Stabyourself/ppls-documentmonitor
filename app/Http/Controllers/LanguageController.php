<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function change(Request $request, $lang) {
        return back()->cookie(
            'lang', $lang, 0
        );
    }
}
