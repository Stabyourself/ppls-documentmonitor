<div id="app">
    <tile-creator
        default-name="{{ $tile->name }}"
        default-type="{{ $tile->type ?? 0 }}"
        default-category="{{ $tile->category_id }}"
        default-properties-json="{{ $tile->properties->pluck("property")->toJson() }}"
        default-permissions-json="{{ $tile->permissions->pluck("uid")->toJson() }}"
        default-repository="{{ $tile->repository ?? '' }}"
        default-icon="{{ $tile->icon ?? "chart-line" }}"
        default-color="{{ $tile->color ?? "#319c32" }}"
        :default-include-outgoing="{{ $tile->include_outgoing ?? 0 }}"

        enabled-types-json="{{ json_encode(config("app.enabled_types")) }}"

        default-display-group="{{ $tile->display_group ?? 0 }}"

        users-url="{{ route("users") }}"
        groups-url="{{ route("groups") }}"
        categories-url="{{ route("categories") }}"
        repositories-url="{{ route("repositories") }}"
        :passworded="{{ (!empty(config("app.security_password"))?"true":"false") }}"
    />
</div>
