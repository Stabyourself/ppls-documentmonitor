<?php

namespace App\Http\Controllers;

use App\Models\Tile;
use Illuminate\Http\Request;
use App\Services\DMSApiService;

class ApiController extends Controller
{
    public static function filterDocuments($documents, $properties)
    {
        $filtered = collect();

        // order document properties
        foreach ($documents as $i => $document) {
            $columns = [];

            // loop through my properties
            foreach ($properties as $property) {
                // loop through received properties
                foreach ($document->sourceProperties as $sourceProperty) {
                    if ($sourceProperty->key == $property->property) {
                        $value = $sourceProperty->value;

                        if ($sourceProperty->isMultiValue && !empty($value)) {
                            $value .= " ...";
                        }

                        $columns[$property->property] = $value;
                        break;
                    }
                }
            }

            $columns["_link"] = $document->_links->self->href; // add link at the end
            $columns["_update"] = $document->_links->update->href; // add link at the end
            if (property_exists($document->_links, "workflow")) {
                $columns["_workflow"] = $document->_links->workflow->href; // add workflow at the end
            }
            if (property_exists($document, "comment")) {
                $columns["_comment"] = $document->comment;
                $columns["_commentLink"] = $document->commentLink;
            }

            $filtered->add($columns);
        }

        return $filtered;
    }

    public static function filterPropertyFields($propertyField)
    {
        if ($propertyField->id == "property_remark") { // weird field that appears as "property_remark1-4" in the DMS results
            return false;
        }

        return true;
    }


    public function users(DMSApiService $DMSApiService)
    {
        $users = $DMSApiService->getUsers();

        $returnUsers = [];
        foreach ($users->resources as $user) {
            $returnUsers[] = [
                "value" => $user->id,
                "name" => $user->displayName,
            ];
        }

        return response()->json($returnUsers);
    }

    public function groups(DMSApiService $DMSApiService)
    {
        $groups = $DMSApiService->getGroups();

        $returnGroups = [];
        foreach ($groups->resources as $group) {
            $returnGroups[] = [
                "value" => $group->id,
                "name" => $group->displayName,
            ];
        }

        return response()->json($returnGroups);
    }

    public function repositories(DMSApiService $DMSApiService)
    {
        $repositories = $DMSApiService->getRepositories();

        $repositories = array_filter($repositories["repositories"], function ($repository) {
            return $repository["available"];
        });

        return response()->json($repositories);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories(DMSApiService $DMSApiService)
    {
        $DMSApiService->repository = request()->get("repository");

        $categories = $DMSApiService->getCategories();
        $filteredCategories = [];

        foreach ($categories->objectDefinitions as $category) {
            $filterOut = false;

            if ($category->objectType == 1) { // dossier
                $filterOut = true;
            }

            if (substr($category->displayName, 0, 1) == "$") { // weird meta types
                $filterOut = true;
            }

            if (substr($category->id, 0, 1) == "$") { // weird meta types
                $filterOut = true;
            }

            if (count($category->propertyFields) == 0) { // empty category
                $filterOut = true;
            }

            if (!$filterOut) {
                // filter out properties that are bad
                $filteredPropertyFields = array_values(array_filter($category->propertyFields, "\App\Http\Controllers\ApiController::filterPropertyFields"));

                // filter out property attributes that we don't need
                $allowed = ["id", "displayName", "dataType"];

                foreach ($filteredPropertyFields as $filteredPropertyField) {
                    foreach ($filteredPropertyField as $k => $v) {
                        if (!in_array($k, $allowed)) {
                            unset($filteredPropertyField->$k);
                        }
                    }
                }

                // Insert workflow location property
                $filteredPropertyFields[] = [
                    "id" => "_workfloweditor",
                    "displayName" => "Workflow Bearbeiter",
                    "dataType" => 0
                ];

                // Insert workflow sender property
                $filteredPropertyFields[] = [
                    "id" => "_workflowsender",
                    "displayName" => "Workflow Starter",
                    "dataType" => 0
                ];

                // Insert workflow comment property
                $filteredPropertyFields[] = [
                    "id" => "_comment",
                    "displayName" => "Workflow Betreff",
                    "dataType" => 0
                ];

                // Insert workflow in/outbound property
                $filteredPropertyFields[] = [
                    "id" => "_datereceived",
                    "displayName" => "Erhalten am",
                    "dataType" => 3
                ];

                // Insert workflow in/outbound property
                $filteredPropertyFields[] = [
                    "id" => "_inboundoutbound",
                    "displayName" => "Quelle",
                    "dataType" => 0
                ];

                // build our output
                $filteredCategories[] = [
                    "id" => $category->id,
                    "displayName" => $category->displayName,
                    "objectType" => $category->objectType,
                    "propertyFields" => $filteredPropertyFields
                ];
            }
        }

        usort($filteredCategories, function ($a, $b) {
            return $a["displayName"] > $b["displayName"];
        });

        return response()->json($filteredCategories);
    }

    public function documents(Request $request, DMSApiService $DMSApiService, Tile $tile)
    {
        $params = request()->all();

        $type = $request->get("type") ?? 2;
        $includeOutbound = $request->get("includeOutbound") ?? false;

        // dvelop api limits us to 1000 size
        // $size = min($size, 1000);
        // $size = max($size, 250);

        $properties = $tile->properties;

        // $documents = $DMSApiService->getDocuments($tile->repository, $tile->category_id, $size);

        $DMSApiService->repository = $tile->repository;

        $preFilterColumn = null;
        $preFilterValue = null;

        if (!empty(config("prefilter.column"))) {
            $preFilterColumn = config("prefilter.column");
            $preFilterValue = "NOGROUP#";

            foreach (session("user")["groups"] as $userGroup) {
                foreach (config("prefilter.groups") as $i => $filterGroup) {
                    if ($userGroup["display"] == $filterGroup) {
                        $preFilterValue = config("prefilter.values")[$i];
                    }
                }
            }
        }

        $categoryId = $tile->category_id;

        if ($categoryId == "-1") {
            $categoryId = null;
        }

        $documents = $DMSApiService->getWorkflowDocuments(
            $categoryId,
            $properties->pluck("property"),
            $params,
            session("user")["id"],
            $preFilterColumn,
            $preFilterValue,
            $type,
            $tile->id,
            $includeOutbound
        );

        return (response()->json($documents));

        $filtered = $this->filterDocuments($documents->items, $properties);


        return response()->json([
            "data" => $filtered->toArray(),
        ]);
    }
}
