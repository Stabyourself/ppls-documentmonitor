@extends("layouts.app")

@php
    $title = "Konfiguration"
@endphp

@section("content")
    <h1 class="mt-3">{{ __("app.create_tile") }}</h1>

    <form method="POST" action="{{ route("tile.store") }}">
    @csrf

    @include ("tile.form")

    <button type="submit" class="btn btn-primary">Speichern</button> <a href="{{ route("tile.index") }}" class="btn btn-outline-secondary">Zurück</a>
@endsection

@push("scripts")
    <script>
        new Vue({
            el: '#app'
        });
    </script>
@endpush
