<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;

function parse_cache_control($cache_control)
{
    $cache_control_array = explode(',', $cache_control);
    $cache_control_array = array_map('trim', $cache_control_array);
    $cache_control_parsed = array();
    foreach ($cache_control_array as $value) {
        if (strpos($value, '=') !== FALSE) {
            $temp = array();
            parse_str($value, $temp);
            $cache_control_parsed += $temp;
        } else {
            $cache_control_parsed[$value] = TRUE;
        }
    }
    return $cache_control_parsed;
}

function redirectToLogin()
{
    return redirect("https://" . request()->header("x-forwarded-server") . "/identityprovider/login?redirect=" . "/ppls-documentmonitor/" . request()->path());
}

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (App::environment(["onprem", "cloud"])) { // skip this middleware on local
            // get the authSessionId
            $authSessionId = $request->cookie("AuthSessionId") ?: $request->header("Authorization");

            if (substr($authSessionId, 0, 7) == "Bearer ") {
                $authSessionId = substr($authSessionId, 7);
            }

            // check if session is valid
            $cachedUser = session("user");

            if (!empty($cachedUser)) {
                // check if the session needs to be invalidated
                if (
                    $cachedUser["authSessionId"] != $authSessionId || // invalid authSessionId
                    $cachedUser["timeout"] < time() // session timeout
                ) {
                    $request->session()->forget("user");
                }
            }

            // do we have a valid cache?
            if (empty(session("user"))) {
                // authorize user
                if (empty($authSessionId)) { // authSessionId not present, we don't even need to attempt to validate
                    return redirectToLogin();
                }

                $response = Http::withOptions(["verify" => false])->withHeaders([
                    "Authorization" => "Bearer " . $authSessionId
                ])->get("https://" . $request->header("x-forwarded-server") . "/identityprovider/validate");

                if ($response->getStatusCode() == 401) { // unauthorized
                    return redirectToLogin();
                }

                $cacheControl = parse_cache_control($response->header("Cache-Control"));

                $user = $response->json();
                $user["authSessionId"] = $authSessionId;
                $user["timeout"] = time() + intval($cacheControl["max-age"]);

                session(['user' => $user]);
            }
        } else {
            // put a dummy user in session for testing
            $user = json_decode('{
                "schemas": ["urn:scim:schemas:core:1.0"],
                "id": "8bb44879-14ff-4998-80f0-923a75ea95c6",
                "userName": "PAPERLESSGROUP\\\\Guegan",
                "name": {
                    "familyName": "Guegan",
                    "givenName": "Maurice"
                },
                "displayName": "Guegan, Maurice",
                "emails": [{
                    "value": "Maurice.Guegan@ppls.de"
                }, {
                    "value": "Guegan@paperless.onmicrosoft.com"
                }],
                "groups": [{
                    "value": "264be4e3-4da2-42eb-ab9d-19037de387be",
                    "display": "secProjekt"
                }, {
                    "value": "cfe4fa76-bf07-4af7-bb9b-224d57612bbc",
                    "display": "alle"
                }, {
                    "value": "cc091b42-f07d-4e21-8e5e-bc7401334bbe",
                    "display": "secD3-Entwicklung"
                }, {
                    "value": "fc43245f-b804-4db9-96db-de3bbabc848a",
                    "display": "secD3-Projekte"
                }, {
                    "value": "591687a9-259a-4191-b1c4-62a595ab5a80",
                    "display": "Cloud"
                }, {
                    "value": "9069e706-6668-466f-8659-81004d12a211",
                    "display": "Projekte (E83FD99E)"
                }, {
                    "value": "9daadd63-d696-4335-839a-e19eb0c04283",
                    "display": "d3demo_d3oneadmin"
                }, {
                    "value": "11a96424-7c42-441d-9000-7a194536589a",
                    "display": "secNAS"
                }, {
                    "value": "616f2e93-cc3b-4482-806b-3e42b552a065",
                    "display": "eBuero"
                }, {
                    "value": "98aeb357-7258-4971-819f-1d581885e4c8",
                    "display": "Mobile User"
                }, {
                    "value": "44a11468-4327-44db-bddc-01aacdd3e0d6",
                    "display": "MS-Mauser"
                }, {
                    "value": "55a122c9-9c06-4ad5-9802-50cbf59da935",
                    "display": "Managed Service"
                }, {
                    "value": "4ff86233-f146-4c91-9638-883be201ea72",
                    "display": "Alle Benutzer"
                }, {
                    "value": "3e379abf-dc10-4d06-a660-6983e9af1be0",
                    "display": "PPLS-intern"
                }, {
                    "value": "DC4885EF-A72C-4489-95A1-F37269D6E48D",
                    "display": "GlobalAdminGroup"
                }, {
                    "value": "6DB690CB-EA1B-4D45-B00B-63A2E7B21816",
                    "display": "Administrative group for the tenant"
                }],
                "photos": [{
                    "value": "\/identityprovider\/scim\/photo\/5a812830-0954-4180-ad25-78eaf832d4f3",
                    "type": "photo"
                }],
                "authSessionId": "invalid"
            }', true);

            session(["user" => $user]);
        }

        return $next($request);
    }
}
