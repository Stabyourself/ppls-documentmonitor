<?php

namespace App\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class DMSApiService
{
    public function __construct()
    {
        $this->token = config("d3.token"); // demo.paperlesscloud.de
        $this->url = config("d3.url");
        $this->repository = null;
        $this->language = App::getLocale();
    }

    public function getHeaders()
    {
        return [
            'Authorization'     => 'Bearer ' . $this->token,
            'Accept'            => 'application/json',
            'Accept-Language'   => $this->language,
        ];
    }

    public function getUsers()
    {
        $response = Http::withHeaders($this->getHeaders())->withOptions(["verify" => false])->get(
            $this->url . "/identityprovider/scim/Users"
        );

        return json_decode($response->getBody());
    }

    public function getGroups()
    {
        $response = Http::withHeaders($this->getHeaders())->withOptions(["verify" => false])->get(
            $this->url . "/identityprovider/scim/Groups"
        );

        return json_decode($response->getBody());
    }

    public function getRepositories()
    {
        $response = Http::withHeaders($this->getHeaders())->withOptions(["verify" => false])->get(
            $this->url . "/dms/r"
        );

        return json_decode($response->getBody(), true);
    }

    public function getCategories()
    {
        $response = Http::withHeaders($this->getHeaders())->withOptions(["verify" => false])->get(
            $this->url . "/dms/r/{$this->repository}/objdef"
        );

        return json_decode($response->getBody());
    }

    public function getDocuments($category_id, $pageSize)
    {
        $response = Http::withHeaders($this->getHeaders())->withOptions(["verify" => false])->get(
            $this->url . "/dms/r/{$this->repository}/srm",
            [
                "sourceid" => "/dms/r/{$this->repository}/source",
                "sourcecategories" => "[\"{$category_id}\"]",
                "pagesize" => $pageSize
            ]
        );

        return json_decode($response->getBody());
    }

    public function getWorkflowDocuments($category_id, $properties, $params, $userId, $preFilterColumn, $preFilterValue, $type = 2, $id, $includeOutbound)
    {
        $requestParams = array_merge([
            "repository" => $this->repository,
            "sourcecategory" => $category_id,
            "properties" => json_encode($properties),
            "user_id" => $userId,
            "prefilter_column" => $preFilterColumn,
            "prefilter_value" => $preFilterValue,
            "type" => $type,
            "id" => $id,
            "include_outbound" => $includeOutbound
        ], $params);

        $response = Http::withHeaders($this->getHeaders())->withOptions(["verify" => false])->get(
            config("app.workflow_api_url"),
            $requestParams
        );

        // die("<a href={$response->transferStats->getEffectiveUri()}>workflowapi url</a>");

        if ($response->failed()) {
            dump($response);
            print($response);

            abort(500);
        }

        $obj = json_decode($response->getBody());

        if (!$obj) {
            die((string) $response);
        }

        return $obj;
    }
}
