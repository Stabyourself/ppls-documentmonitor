@extends("layouts.app")

@php
    $title = "Konfiguration"
@endphp

@section("content")
    <h1 class="mt-3">Kachel Bearbeiten</h1>
    <form method="POST" action="{{ route("tile.update", $tile) }}">
    @method ("PATCH")
    @csrf

    @include ("tile.form")

    <button type="submit" class="btn btn-primary">Speichern</button> <a href="{{ route("tile.index") }}" class="btn btn-outline-secondary">Zurück</a>
@endsection

@push("scripts")
    <script>
        new Vue({
            el: '#app'
        });
    </script>
@endpush
