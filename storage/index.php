<!DOCTYPE html>
<html lang="de">

<head>
   <!-- Required meta tags -->
   <meta charset="UTF-8">
   <title>RechnungsMonitor 1.0</title>
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
</head>

<body>
   <?php
$myServer = "10.5.0.4";
$myUser = "D3Master";
$myPass = "Nopaper1!";
$myDB = "D3T";
$connectionInfo = array("Database"=>$myDB, "UID" => $myUser, "PWD" => $myPass);

$conn = sqlsrv_connect($myServer, $connectionInfo); //returns false
if( $conn === false )
{
   echo "failed connection";
}

$sql = "SELECT doku_id
             , dok_dat_feld_28 + ' ' + dok_dat_feld_21 AS KUNDE
             , dok_dat_feld_22 AS TODO
             , dok_dat_feld_23 AS SERVER
             , dok_dat_feld_24 AS START
             , dok_dat_feld_28 AS ENDE
             , dok_dat_feld_27 AS DETAIL
             , dok_dat_feld_42 AS BEARBEITER
             , dok_dat_feld_50 AS DATUM
        FROM
             firmen_spezifisch
       WHERE
             kue_dokuart = 'DSLAB'" ;
$result = sqlsrv_query($conn,$sql);
if(sqlsrv_fetch($result) === false)
{
   echo "couldn't fetch data";
}

function meine_funktion() {
   // Deine PHP-Funktion, z. B.:
   echo 'Hallo Welt!';
}

if(isset($_POST["ausfuehren"])) {
      meine_funktion();
}

if(isset($_POST["inmo_heute"])) {
      inmo_heute();
}

function inmo_heute(){
   $myServer = "10.5.0.4";
   $myUser = "D3Master";
   $myPass = "Nopaper1!";
   $myDB = "D3T";
   $connectionInfo = array("Database"=>$myDB, "UID" => $myUser, "PWD" => $myPass);

   $conn = sqlsrv_connect($myServer, $connectionInfo); //returns false
   if( $conn === false )
   {
      echo "failed connection";
   }

   $sql = "SELECT doku_id
                , dok_dat_feld_28 + ' ' + dok_dat_feld_21 AS KUNDE
                , dok_dat_feld_22 AS TODO
                , dok_dat_feld_23 AS SERVER
                , dok_dat_feld_24 AS START
                , dok_dat_feld_28 AS ENDE
                , dok_dat_feld_27 AS DETAIL
                , dok_dat_feld_42 AS BEARBEITER
                , dok_dat_feld_50 AS DATUM
           FROM
                firmen_spezifisch
          WHERE
                kue_dokuart = 'DSLAB'
            AND
                dok_dat_feld_50 >= convert(datetime,floor(convert(float,getdate()-1)))" ;
   $result = sqlsrv_query($conn,$sql);
   if(sqlsrv_fetch($result) === false)
   {
      echo "couldn't fetch data";
   }
}

?>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index.html">
         <img src="img/InvoiceMonitor.png" width="30" height="30" class="d-inline-block align-top" alt="">
         Rechnungs-Monitor</a>
      <button class="navbar-toggler" type="button" data- toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
         <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
               <a class="nav-link" href="#start">Meine Rechnungen<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#">Übersicht (alle)</a>
            </li>
            <li class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Datumsfilter
               </a>
               <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" type="submit" name="inmo_heute">nur heute</a>
                  <a class="dropdown-item" href="#start">seit gestern</a>
                  <a class="dropdown-item" href="#start">letzten 7 Tage</a>
                  <a class="dropdown-item" href="#start">letzten 30 Tage</a>
                  <a class="dropdown-item active" href="#start">kein Filter</a>
               </div>
            </li>
         </ul>
         <button class="btn btn-outline-success my-2 my-sm-0" type="submit">pplsCloudSoloutions</button>
         <!--     <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Suche nach..." aria-label="Search">

         </form> -->
      </div>
   </nav>
   <div class="container-fluid mt-3">
      <table class="table table-striped table-bordered table-hover table-sm table-responsive btn mydatatable" style="width: 100%">
         <thead class="table-dark">
            <tr>
               <th>WF-Status</th>
               <th>RG-Datum</th>
               <th>Rechnungs-Nr.</th>
               <th class="w-25">Kreditor-Name</th>
               <th>WF-Bearbeiter</th>
               <th>RG-Betrag</th>
               <th class="w-25">Währung</th>
               <th>Aktion</th>
            </tr>
         </thead>
         <tbody>
            <?php while($row = sqlsrv_fetch_array($result)) : ?>
            <tr>
               <th><?php echo $row['doku_id']; ?></th>
               <th><?php echo $row['ENDE']; ?></th>
               <th><?php echo $row['SERVER']; ?></th>
               <th><?php echo $row['KUNDE']; ?></th>
               <th><?php echo $row['BEARBEITER']; ?></th>
               <th><?php echo $row['TODO']; ?></th>
               <th><?php echo $row['KUNDE']; ?></th>
               <th>
                  <form action="" method="post">
                     <input type="submit" name="ausfuehren" value="Show" class="btn btn-light" />
                     <a></a>
                     <input type="submit" name="inmo_heute" value="Workflow" class="btn btn-secondary" />
                  </form>
               </th>
            </tr>
            <?php endwhile; ?>
         </tbody>
         <tfoot>
            <tr>
               <th class="clearfix visible-md-block">WF-Status</th>
               <th>RG-Datum</th>
               <th>Rechnungs-Nr.</th>
               <th>Kreditor-Name</th>
               <th>WF-Bearbeiter</th>
               <th>RG-Betrag</th>
               <th>Währung</th>
               <th>Aktion</th>
            </tr>
         </tfoot>
      </table>
   </div>
   <!-- jQuery first, then Tether, then Bootstrap JS. -->
   <script src="js/jquery-3.3.1.min.js"></script>
   <script src="js/popper.min.js"></script>
   <script src="js/bootstrap.min.js"></script>

   <script src="js/jquery.dataTables.min.js"></script>
   <script src="js/dataTables.bootstrap4.min.js"></script>
   <!-- Einsetzbar in d3one
   <script src="js/dapi.js"></script>
   -->
   <script>
      var table = $('.mydatatable').DataTable({
         order: [
            [0, 'desc']
         ],
         lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "Alle"]
         ]
      });

      $('.mydatatable tfoot th').each(function() {
         var title = $(this).text();
         $(this).html('<input type="text" placeholder="Suche ' + title + '"/>');
      });

      table.columns().every(function() {
         var that = this;
         $('input', this.footer()).on('keyup change', function() {
            if (that.search() !== this.value) {
               that.search(this.value).draw();
            }
         });
      });

   </script>

</body>

</html>
<?php sqlsrv_close( $conn ); ?>
