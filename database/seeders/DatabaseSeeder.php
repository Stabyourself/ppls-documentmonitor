<?php

namespace Database\Seeders;

use App\Models\Property;
use App\Models\Tile;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $user = \App\Models\User::factory()->create();

        // \App\Models\Tile::factory(3)->create()->each(function($tile) {
        //     \App\Models\Property::factory(8)->create([
        //         "tile_id" => $tile->id,
        //     ]);
        // });

        // \App\Models\Document::factory(500)->create();

        Tile::factory()->has(Property::factory()->count(rand(5, 10)))->count(50)->create();
    }
}
