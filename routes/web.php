<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
Route::group(['middleware' => ['tenant', 'language']], function () {
    Route::group(['middleware' => ["login"]], function () {
        Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
        Route::get('/homefeatures', [App\Http\Controllers\HomeController::class, 'homeFeatures'])->name("homefeatures");
        Route::get('/configfeatures', [App\Http\Controllers\HomeController::class, 'configFeatures'])->name("configfeatures");
        Route::get('/sources', [App\Http\Controllers\HomeController::class, 'sources'])->name("sources");
        Route::get('/lang/{lang}', [App\Http\Controllers\LanguageController::class, 'change'])->name("lang");

        Route::get('/test', [App\Http\Controllers\TestController::class, 'test'])->name("test");

        Route::get('/tile/new', [App\Http\Controllers\TileController::class, 'create'])->name("tile.create");
        Route::post('/tile', [App\Http\Controllers\TileController::class, 'store'])->name("tile.store");

        Route::get('/tile', [App\Http\Controllers\TileController::class, 'index'])->name("tile.index");
        Route::get('/tile/all', [App\Http\Controllers\TileController::class, 'showAll'])->name("tile.showAll");
        Route::get('/tile/users', [App\Http\Controllers\TileController::class, 'showUsers'])->name("tile.showUsers");
        Route::get('/tile/{tile}', [App\Http\Controllers\TileController::class, 'show'])->name("tile.show");

        Route::delete('/tile/{tile}', [App\Http\Controllers\TileController::class, 'destroy'])->name("tile.destroy");

        Route::get('/tile/{tile}/edit', [App\Http\Controllers\TileController::class, 'edit'])->name("tile.edit");
        Route::patch('/tile/{tile}', [App\Http\Controllers\TileController::class, 'update'])->name("tile.update");

        // API
        Route::get('/users', [App\Http\Controllers\ApiController::class, 'users'])->name("users");
        Route::get('/groups', [App\Http\Controllers\ApiController::class, 'groups'])->name("groups");
        Route::get('/categories', [App\Http\Controllers\ApiController::class, 'categories'])->name("categories");
        Route::get('/repositories', [App\Http\Controllers\ApiController::class, 'repositories'])->name("repositories");
        Route::get('/documents/{tile}', [App\Http\Controllers\ApiController::class, 'documents'])->name("documents");
        Route::get('/documents/users/{user}', [App\Http\Controllers\ApiController::class, 'documentsUsers'])->name("documents/users");
    });
});
