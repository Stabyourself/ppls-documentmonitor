@php
    $title = $tile->name;
    $navbarColor = $tile->color;
    $icon = $tile->icon;
@endphp

@extends("layouts.app")

@section("content")
    <div class="row">
        <div id="app">
            <tile-grid
                ref="grid"
                ajax-url="{{ route("documents", $tile->id) }}"
                :properties='{!! json_encode($properties) !!}'
                tile-id="{{ $tile->id }}"
                :type="{{ $tile->type }}"
                locale="{{ App::getLocale() }}"
                name="{{ $tile->name }}"
                repository="{{ $tile->repository }}"
                :include-outbound="{{ $tile->include_outgoing }}"
                :property-groupings='{!! json_encode(config("app.property_groupings")) !!}'
            />
        </div>
    </div>
@endsection

@push("scripts")
<script>
    let vue = new Vue({
        el: '#app'
    });

    if (typeof dapi !== "undefined") {
        let firstTime = true;

        dapi.setResourceVisibilityChangedCallback(visible => {
            if (visible) {
                if (firstTime) {
                    firstTime = false;
                } else {
                    vue.$refs.grid.refreshDataGrid()
                }
            }
        })
    }
</script>
@endpush
