@extends("layouts.app")

@php
    $title = "Konfiguration";
@endphp

@section("content")
    <h1 class="mt-3">
        Meine Kacheln
        <span class="small">
            <a href="{{ route("tile.create") }}" class="btn btn-outline-primary" style="vertical-align: text-top">
                <i class="fas fa-plus"></i>
            </a>
        </span>
    </h1>

    <div class="row">
        @forelse ($tiles as $tile)
            <div class="col-lg-3 col-md-6">
                <div class="card mb-4 shadow-sm">
                    <div class="card-body">
                        <h5 class="card-title">{{ $tile->name }}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{ $tile->category_name }}</h6>
                        <p class="card-text">
                            <ul>
                                @foreach ($tile->properties as $property)
                                    <li>
                                        {{ $property->name }}
                                    </li>
                                @endforeach
                            </ul>
                        </p>
                        <a href="{{ route("tile.edit", $tile) }}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Bearbeiten</a>
                        <a data-toggle="modal" data-target="#deleteModal" class="btn btn-outline-danger tile-delete-button" data-delete-href="{{ route('tile.destroy', $tile) }}" data-name="{{ $tile->name }}"><i class="fas fa-trash-alt"></i> Löschen</a>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-lg-3 col-md-6">
                <a href="{{ route("tile.create") }}" class="card-link">
                    <div class="card card-placeholder">
                        <div class="card-body">
                            <h5 class="text-muted mb-0">
                                Noch keine Kacheln da. Jetzt eine anlegen!
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
        @endforelse

    </div>


    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="delete-form" method="POST">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Kachel Löschen</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Möchten Sie die Kachel <span id="tile-name"></span> wirklich löschen?
                        </p>

                        @if (!empty(config("app.security_password")))
                            <div class="form-group">
                                <label for="password">Passwort</label>
                                <input class="form-control" id="password" name="password" v-model="password">
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">Kachel Löschen</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <p class="text-muted">Version: {{ config("app.version") }}</p>
@endsection

@push ("scripts")
    <script>
        $(".tile-delete-button").on("click", function() {
            let href = $(this).data("delete-href")
            let name = $(this).data("name")

            $("#delete-form").attr('action', href)
            $("#tile-name").html(name)
        })
    </script>
@endpush