<?php

return [
    "column" => env("PREFILTER_COLUMN"),
    "groups" => explode(',', env('PREFILTER_GROUPS')),
    "values" => explode(',', env('PREFILTER_VALUES')),
];
