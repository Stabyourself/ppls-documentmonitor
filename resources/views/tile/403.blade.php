@php
    $title = "Zugriff verweigert"
@endphp

@extends("layouts.app")

@section("content")
    <div class="row justify-content-center">
        <div class="card mt-3" style="width: 24rem;">
            <div class="card-body">
                <h5 class="card-title">Zugriff verweigert</h5>
                <p class="card-text">Sie haben nicht die Berechtigung, diese Kachel anzusehen.</p>
                <p class="card-text">Bitte kontaktieren Sie Ihren Administrator.</p>
            </div>
        </div>
    </div>
@endsection