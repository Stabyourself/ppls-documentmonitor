@php
    $title = "Postkorb"
@endphp

@extends("layouts.app")

@push("scripts")
    <script>
        var vues = [];
    </script>
@endpush

@section("content")
    <div class="row">
        <div id="accordion">
            @foreach ($tiles as $i=>$tile)
                <div id="app{{ $i }}">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <button class="btn btn-link" :class="{'font-weight-bold': count > 0}" data-toggle="collapse" data-target="#collapse{{ $i }}">
                                    <i class="fas fa-inbox mr-2"></i>{{ $tile->name }} <span v-cloak>(@{{ count }})</span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapse{{ $i }}" class="collapse" data-parent="#accordion">
                            <tile-grid
                                ref="grid"
                                @rowcountchanged="rowCountChanged"
                                ajax-url="{{ route("documents", $tile->id) }}"
                                :properties='{!! $tile->properties->toJson() !!}'
                                tile-id="{{ $tile->id }}"
                                :type="{{ $tile->type }}"
                                locale="{{ App::getLocale() }}"
                                :include-outbound="{{ $tile->include_outgoing }}"
                                :property-groupings='{!! json_encode(config("app.property_groupings")) !!}'
                            />
                        </div>
                    </div>
                </div>

                @push("scripts")
                    <script>
                        vues.push(new Vue({
                            el: '#app{{ $i }}',
                            data: {
                                count: "Lade...",
                            },
                            methods: {
                                rowCountChanged(count) {
                                    this.count = count
                                }
                            }
                        }));
                    </script>
                @endpush
            @endforeach
        </div>
    </div>
@endsection

@push ("scripts")
<script>
    if (typeof dapi !== "undefined") {
        let firstTime = true;

        dapi.setResourceVisibilityChangedCallback(visible => {
            if (visible) {
                if (firstTime) {
                    firstTime = false;
                } else {
                    for (vue of vues) {
                        vue.$refs.grid.refreshDataGrid()
                    }
                }
            }
        })
    }
</script>
@endpush