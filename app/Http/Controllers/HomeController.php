<?php

namespace App\Http\Controllers;

use App\Models\Tile;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) { // API call by d.velop
            $info = [
                "_links" => [
                    "featuresdescription" => [
                        "href" => "/ppls-documentmonitor/homefeatures",
                    ],

                    "configfeatures" => [
                        "href" => "/ppls-documentmonitor/configfeatures",
                    ]
                ]
            ];

            return response()->json($info);
        } else { // regular call
            return view("home.index");
        }
    }

    public function homeFeatures()
    {
        $user = session("user");
        $info = ["features" => []];

        foreach (\App\Models\Tile::where("display_group", "=", 0)->get() as $tile) {

            if ($tile->userCanSee($user)) {
                $info["features"][] = [
                    "url" => "/ppls-documentmonitor/tile/" . $tile->id,
                    "title" => $tile->name,
                    "subtitle" => $tile->subtitle,
                    "iconURI" => $tile->getIconUrl(),
                    "summary" => $tile->subtitle,
                    "description" => $tile->description,
                    "color" => $tile->color,
                ];
            }
        }

        if (\App\Models\Tile::where("display_group", "=", 1)->count() > 0) {
            $info["features"][] = [
                "url" => "/ppls-documentmonitor/tile/all",
                "title" => "Postkorb Manager",
                "subtitle" => "Immer aktuell",
                "iconURI" => "/ppls-documentmonitor/icons/chart-line.svg",
                "summary" => "Immer aktuell",
                "description" => "Dokument Monitoring",
                "color" => "#064769",
            ];
        }

        return response()->json($info);
    }

    public function configFeatures()
    {
        $info = [];

        $hasPermission = true;
        $user = session("user");

        if (!empty(config("app.config_group"))) {
            $hasPermission = false;

            foreach ($user["groups"] as $group) {
                if (config("app.config_group") == $group["display"]) {
                    $hasPermission = true;
                    break;
                }
            }
        }

        if ($hasPermission) {
            $info = [
                "appName" => "ppls document monitor",
                "customHeadlines" => [
                    [
                        "caption" => "Dokument Monitor",
                        "description" => "Konfiguration der Dokument Monitor Kacheln",
                        "menuItems" => [
                            [
                                "caption" => "Kachel Assistent",
                                "description" => "Anlegen und bearbeiten von Dokument Monitor Kacheln",
                                "href" => "/ppls-documentmonitor/tile",
                                "keywords" => [
                                    "Kachel",
                                    "Dokument",
                                    "Monitor",
                                    "Monitoring"
                                ],
                                "configurationState" => 0
                            ]
                        ]
                    ]
                ]
            ];
        }

        return response()->json($info);
    }
}
