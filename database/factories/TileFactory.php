<?php

namespace Database\Factories;

use App\Models\Tile;
use Illuminate\Database\Eloquent\Factories\Factory;

class TileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => ucwords($this->faker->word) . " Monitor",
            "type" => 0,
            "repository" => $this->faker->uuid,
            "category_id" => $this->faker->word,
            "category_name" => $this->faker->word,
            "icon" => "chart-bar",
            "color" => "#064769",
            "subtitle" => $this->faker->words(8, true),
            "description" => $this->faker->words(20, true),
        ];
    }
}
