<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Property extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = ["name", "data_type", "property", "order", "tile_id"];

    public $translatable = ["name"];

    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            $attributes[$field] = $this->getTranslation($field, \App::getLocale());
        }
        return $attributes;
    }

    public function tile()
    {
        return $this->belongsTo("App\Models\Tile");
    }

    public function getKey()
    {
        return "property_{$this->tile->id}_{$this->id}";
    }
}
