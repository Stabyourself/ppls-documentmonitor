# 2.5
- Unterstützung für eigene Icons
- Ändern der Farbe von Kacheln
- Die Leiste am oberen Bildschirmrand passt sich ebenfalls an

# 2.4
- Fügt die Möglichkeit hinzu, ein Passwort zu setzen
- Dieses Passwort wir benötigt, um Kacheln anzulegen, bearbeiten, oder zu löschen

# 2.3
- Verbessern der Geschwindigkeit bei vielen (>1000) Treffern
- Fix für Workflows

# 2.2
- Fügt die "Alle" Kachel hinzu
- Erlaubt es, mehrere Archive zu definieren

# 2.1
- Multi-Sprachen Support

# 2.0
- Ursprünglicher Release