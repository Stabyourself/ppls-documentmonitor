<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TilePermission extends Model
{
    protected $fillable = [
        "tile_id",
        "uid",
        "type",
    ];
}
