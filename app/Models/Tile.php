<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Tile extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        "name",
        "type",
        "repository",
        "category_id",
        "category_name",
        "color",
        "subtitle",
        "description",
        "icon",
        "display_group",
        "include_outgoing",
    ];

    public $translatable = ["category_name"];

    public function properties()
    {
        return $this->hasMany("App\Models\Property")->orderBy("order");
    }

    public function getUrl()
    {
        return route("tile.show", $this);
    }

    public function getIconUrl()
    {
        return "/ppls-documentmonitor/icons/{$this->icon}.svg";
    }

    public function permissions()
    {
        return $this->hasMany("App\Models\TilePermission");
    }

    public function userCanSee($user)
    {
        if ($this->permissions->count() == 0) {
            return true;
        }

        if ($this->permissions->count() > 0) {
            foreach ($this->permissions as $permission) {
                if ($permission->uid == $user["id"]) {
                    return true;
                }

                foreach ($user["groups"] as $group) {
                    if ($permission->uid == $group["value"]) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
