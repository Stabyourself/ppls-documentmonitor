<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? "Dokument Monitor" }}</title>

    <!-- Styles -->
    <link rel="icon" href="{{ asset("img/favicon.png") }}">
    <link href="{{ asset(mix("css/app.css")) }}" rel="stylesheet">

    @if (App::environment(["onprem", "production"]))
        <script src="{{ asset(mix("js/dapi.js")) }}"></script>
    @endif
</head>
<body>
    <nav class="navbar navbar-expand navbar-dark bg-primary"{!! !empty($navbarColor) ? ' style="background-color: ' . $navbarColor . ' !important"' : '' !!}>
        <div class="navbar-brand">
            <img src="{{ empty($icon) ? asset("img/icon.svg") : asset("icons/" . $icon . ".svg") }}" width="30" height="30" class="d-inline-block align-top" alt="">
            {{ $title ?? "Dokument Monitor" }}
        </div>

        <div class="collapse navbar-collapse mr-2" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ strtoupper(Cookie::get("lang") ?: config("app.languages")[0]) }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach (config("app.languages") as $lang)
                            <a class="dropdown-item" href="{{ route("lang", $lang) }}">{{ strtoupper($lang) }}</a>
                        @endforeach
                    </div>
                </li>
            </ul>
        </div>

        <a class="ml-auto btn btn-outline-light my-2 my-sm-0" target="_blank" href="https://www.paperless-solutions.de/">pplsCloudSolutions</a>
    </nav>

    @if(Session::has('message'))
        <div class="d-flex justify-content-center align-items-center" style="position: absolute; left: 50%;">
            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" style="position: relative; left: -50%;">
                <div class="toast-header bg-{{ Session::get('message-class', 'success') }}">
                    <strong class="mr-auto">Dokument Monitor</strong>

                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="toast-body">
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>
    @endif

    <div class="container-fluid">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset(mix("js/app.js")) }}"></script>
    @stack('scripts')
</body>
</html>
