@extends("layouts.app")

@php
    $title = "Test"
@endphp

@section("content")
    <div id="app">
        <tile-grid ajax-url="{{ route("documents", 4) }}" :properties='{!! App\Models\Tile::find(4)->properties->toJson() !!}' />
    </div>
@endsection

@push("scripts")
    <script>
        new Vue({
            el: '#app'
        });
    </script>
@endpush
