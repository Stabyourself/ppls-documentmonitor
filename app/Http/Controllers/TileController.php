<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Tile;
use App\Models\Property;
use App\Models\TilePermission;
use App\Services\DMSApiService;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class TileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiles = Tile::all();

        return view("tile.index")->with(compact("tiles"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tile = new Tile();

        return view("tile.create")->with(compact("tile"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DMSApiService $dMSApiService)
    {
        $validated = $request->validate([
            "password" => "",
            "name" => "required|max:255",
            "type" => "required|in:0,1,2",
            "repository_id" => "required",
            "category_id" => "required",
            'properties' => 'required|array',
            'permissions' => 'array',
            'permission_types' => 'array',
            'display_group' => 'integer',
            'icon' => "required",
            'color' => "required",
            "include_outgoing" => "boolean",
        ]);

        $validated["include_outgoing"] = boolval($validated["include_outgoing"] ?? null);

        if (!empty(config("app.security_password")) && $validated["password"] !== config("app.security_password")) {
            Session::flash('message', 'Falsches Passwort');
            Session::flash('message-class', 'danger');
            return back();
        }

        // get category name in all languages
        $categoryName = [];
        $propertyNames = [];
        $propertyTypes = [];

        $dMSApiService->repository = $validated["repository_id"];

        foreach (config("app.languages") as $language) {
            $dMSApiService->language = $language;

            $categories = $dMSApiService->getCategories();

            foreach ($categories->objectDefinitions as $category) {
                if ($category->id == $validated["category_id"] || $validated["category_id"] == "-1") {
                    $categoryName[$language] = $category->displayName;

                    $propertyFields = $category->propertyFields;

                    if ($validated["category_id"] == "-1") {
                        $categoryName[$language] = "Alle Kategorien";
                        $propertyFields = config("app.general_properties");
                    }

                    // get all property names
                    foreach ($propertyFields as $property) {
                        $property_i = array_search($property->id, $validated["properties"]);
                        if ($property_i !== false) {
                            // we now have a property we want
                            $property_id = $validated["properties"][$property_i];

                            if (!isset($propertyNames[$property_id])) { // create the language table if it doesn't exist yet
                                $propertyNames[$property_id] = [];
                            }

                            $propertyNames[$property_id][$language] = $property->displayName;
                            $propertyTypes[$property_id] = $property->dataType;
                        }
                    }


                    break;
                }
            }
        }

        $tile = Tile::create([
            "name" => $validated["name"],
            "type" => $validated["type"],
            "repository" => $validated["repository_id"],
            "category_id" => $validated["category_id"],
            "category_name" => $categoryName,
            "color" => $validated["color"],
            "subtitle" => config("app.tile_subtitle"),
            "description" => "Dokument Monitoring",
            "icon" => $validated["icon"],
            "display_group" => $validated["display_group"],
            "include_outgoing" => $validated["include_outgoing"],
        ]);

        // create properties
        foreach ($validated["properties"] as $i => $property) {
            if ($property == "_workfloweditor") {
                Property::create([
                    "name" => "Workflow Bearbeiter",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_workflowsender") {
                Property::create([
                    "name" => "Workflow Starter",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_comment") {
                Property::create([
                    "name" => "Workflow Betreff",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_inboundoutbound") {
                Property::create([
                    "name" => "Quelle",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_datereceived") {
                $type = 3;

                if (isset($validated["withtimes"][$i])) {
                    $type = 3;
                } else {
                    $type = 4;
                }

                Property::create([
                    "name" => "Erhalten am",
                    "property" => $property,
                    "data_type" => $type,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } else {
                Property::create([
                    "name" => $propertyNames[$property],
                    "property" => $property,
                    "data_type" => $propertyTypes[$property],
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            }
        };

        // create permissions
        if (array_key_exists("permissions", $validated)) {
            foreach ($validated["permissions"] as $i => $uid) {
                TilePermission::create([
                    "tile_id" => $tile->id,
                    "uid" => $uid,
                    "type" => $validated["permission_types"][$i],
                ]);
            };
        }

        Session::flash('message', 'Kachel ' . $tile->name . ' wurde erfolgreich angelegt!');

        return redirect(route("tile.index"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tile $tile
     * @return \Illuminate\Http\Response
     */
    public function show(Tile $tile)
    {
        // check permissions
        if (!$tile->userCanSee(session("user"))) {
            return response()->view("tile.403", [], 403);
        }

        $properties = $tile->properties->toArray();

        return view("tile.show")->with(compact("tile", "properties"));
    }

    public function showAll()
    {
        $tiles = Tile::where("display_group", "=", 1)->get();
        return view("tile.showAll")->with(compact("tiles"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tile $tile
     * @return \Illuminate\Http\Response
     */
    public function edit(Tile $tile)
    {
        return view("tile.edit")->with(compact("tile"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tile $tile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tile $tile, DMSApiService $dMSApiService)
    {

        $validated = $request->validate([
            "password" => "",
            "name" => "required|max:255",
            "type" => "required|in:" . implode(",", config("app.enabled_types")),
            "repository_id" => "required",
            "category_id" => "required",
            'properties' => 'required|array',
            'withtimes' => 'array',
            'permissions' => 'array',
            'permission_types' => 'array',
            'display_group' => 'integer',
            'icon' => "required",
            'color' => "required",
            "include_outgoing" => "boolean",
        ]);

        $validated["include_outgoing"] = boolval($validated["include_outgoing"] ?? null);

        if (!empty(config("app.security_password")) && $validated["password"] !== config("app.security_password")) {
            Session::flash('message', 'Falsches Passwort');
            Session::flash('message-class', 'danger');
            return back();
        }

        // get category name in all languages
        $categoryName = [];
        $propertyNames = [];
        $propertyTypes = [];

        $dMSApiService->repository = $validated["repository_id"];

        foreach (config("app.languages") as $language) {
            $dMSApiService->language = $language;

            $categories = $dMSApiService->getCategories();

            foreach ($categories->objectDefinitions as $category) {
                if ($category->id == $validated["category_id"] || $validated["category_id"] == "-1") {
                    $categoryName[$language] = $category->displayName;

                    $propertyFields = $category->propertyFields;

                    if ($validated["category_id"] == "-1") {
                        $categoryName[$language] = "Alle Kategorien";
                        $propertyFields = config("app.general_properties");
                    }

                    // get all property names
                    foreach ($propertyFields as $property) {
                        $property_i = array_search($property->id, $validated["properties"]);
                        if ($property_i !== false) {
                            // we now have a property we want
                            $property_id = $validated["properties"][$property_i];

                            if (!isset($propertyNames[$property_id])) { // create the language table if it doesn't exist yet
                                $propertyNames[$property_id] = [];
                            }

                            $propertyNames[$property_id][$language] = $property->displayName;
                            $propertyTypes[$property_id] = $property->dataType;

                            if ($property->dataType == 3 || $property->dataType == 4) {
                                if (isset($validated["withtimes"][$property_i])) {
                                    $propertyTypes[$property_id] = 3;
                                } else {
                                    $propertyTypes[$property_id] = 4;
                                }
                            }
                        }
                    }

                    break;
                }
            }
        }

        $tile->update([
            "name" => $validated["name"],
            "type" => $validated["type"],
            "repository" => $validated["repository_id"],
            "category_id" => $validated["category_id"],
            "category_name" => $categoryName,
            "color" => $validated["color"],
            "subtitle" => config("app.tile_subtitle"),
            "description" => "Dokument Monitoring",
            "icon" => $validated["icon"],
            "display_group" => $validated["display_group"],
            "include_outgoing" => $validated["include_outgoing"],
        ]);

        // Properties
        $tile->properties()->forceDelete();

        // create properties
        foreach ($validated["properties"] as $i => $property) {
            if ($property == "_workfloweditor") {
                Property::create([
                    "name" => "Workflow Bearbeiter",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_workflowsender") {
                Property::create([
                    "name" => "Workflow Starter",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_comment") {
                Property::create([
                    "name" => "Workflow Betreff",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_inboundoutbound") {
                Property::create([
                    "name" => "Quelle",
                    "property" => $property,
                    "data_type" => 0,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } elseif ($property == "_datereceived") {
                $type = 3;

                if (isset($validated["withtimes"][$i])) {
                    $type = 3;
                } else {
                    $type = 4;
                }

                Property::create([
                    "name" => "Erhalten am",
                    "property" => $property,
                    "data_type" => $type,
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            } else {
                Property::create([
                    "name" => $propertyNames[$property],
                    "property" => $property,
                    "data_type" => $propertyTypes[$property],
                    "order" => $i,
                    "tile_id" => $tile->id,
                ]);
            }
        };

        // Permissions
        $tile->permissions()->forceDelete();

        // create properties
        if (array_key_exists("permissions", $validated)) {
            foreach ($validated["permissions"] as $i => $uid) {
                TilePermission::create([
                    "tile_id" => $tile->id,
                    "uid" => $uid,
                    "type" => $validated["permission_types"][$i],
                ]);
            };
        }

        Session::flash('message', 'Kachel ' . $tile->name . ' wurde erfolgreich bearbeitet!');

        return redirect(route("tile.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tile $tile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tile $tile)
    {
        $validated = $request->validate([
            "password" => "",
        ]);

        if (!empty(config("app.security_password")) && $validated["password"] !== config("app.security_password")) {
            Session::flash('message', 'Falsches Passwort');
            Session::flash('message-class', 'danger');
            return back();
        }

        $tile->forceDelete();

        return redirect(route("tile.index"));
    }
}
