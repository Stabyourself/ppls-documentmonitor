<?php

namespace Database\Factories;

use App\Models\Document;
use Illuminate\Database\Eloquent\Factories\Factory;

class DocumentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Document::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "status" => $this->faker->randomElement(["Bearbeitung", "Abgeschlossen"]),
            "date" => $this->faker->dateTimeThisYear,
            "rechnungsnr" => $this->faker->randomNumber,
            "kreditor" => $this->faker->word,
            "bearbeiter" => $this->faker->name,
            "betrag" => $this->faker->randomFloat(2, 1, 10000),
            "currency" => $this->faker->randomElement(["EUR", "GBP", "USD"]),
        ];
    }
}
